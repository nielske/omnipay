<?php

namespace Nepp95\Omnipay\Commands;

use Illuminate\Console\GeneratorCommand;

class SisowMakeCommand extends GeneratorCommand
{
    protected $name = 'payment:sisow';
    protected $description = 'Create an omnipay controller';
    protected $type = 'PaymentController';

    protected function getStub()
    {
        return __DIR__ . '../stubs/SisowController.stub';
    }

    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\Controllers';
    }
}
