<?php

namespace Nepp95\Omnipay\Commands;

use Illuminate\Console\GeneratorCommand;

class DefaultMakeCommand extends GeneratorCommand
{
    protected $name = 'make:payment';
    protected $description = 'Create an omnipay controller';
    protected $type = 'PaymentController';

    protected function getStub()
    {
        return dirname(__DIR__) . '/stubs/DefaultController.stub';
    }

    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\Http\Controllers';
    }
}
