<?php

/* 
    env variables:
        OMNIPAY_TESTMODE
        OMNIPAY_GATEWAY
        SISOW_SHOPID
        SISOW_MERCHANTID
        SISOW_MERCHANTKEY
*/

return [
    // Global
    'webhookRoute' => route('webhook'),
    'testMode' => env('OMNIPAY_TESTMODE', false),
    'gateway' => env('OMNIPAY_GATEWAY', 'default'),

    // Sisow
    'sisow' => [
        'shopId' => env('SISOW_SHOPID', 12345),
        'merchantId' => env('SISOW_MERCHANTID', 12345),
        'merchantKey' => env('SISOW_MERCHANTKEY', 'YOUR_MERCHANTKEY_HERE'),
        'testMode' => env('OMNIPAY_TESTMODE', false)
    ]
];
