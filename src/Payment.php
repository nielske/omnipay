<?php

namespace Nepp95\Omnipay;

use \Omnipay\Omnipay as Omni;

class Payment
{
    private $defaultGateway;
    private $gateways;
    private $response;
    private $currentGateway;
    private $omnipay;

    public function __construct()
    {
        $this->gateways = [
            'sisow'
        ];

        $this->defaultGateway = config('omnipay.gateway', 'Sisow');
        $this->setGateway();
    }

    /**
     * Set current gateway and create Omnipay object
     */
    public function setGateway($gateway = null)
    {
        if (in_array($gateway ? $gateway : $this->defaultGateway, $this->gateways)) {
            $this->currentGateway = $gateway;
            $this->omnipay = Omni::create($this->getGateway());
            return true;
        }

        return false;
    }

    /**
     * Get current gateway
     */
    public function getGateway()
    {
        return $this->currentGateway;
    }

    /**
     * Initalize omnipay with gateway specific options
     */
    public function initialize($options = null)
    {
        if ($this->omnipay->initialize($options ? $options : config('omnipay.' . $this->getGateway()))) {
            return true;
        };

        return false;
    }

    /**
     * Pay with payment options (amount, account etc)
     */
    public function pay($options, $controller)
    {
        if (!in_array('notifyUrl', $options)) {
            array_push($options, config('omnipay.webhookRoute'));
        }

        try {
            $this->response = $this->omnipay->purchase(
                $options
            )->send();

            return $this->response;

            if ($this->response->isSuccessful())
                call_user_func($controller . '::isSuccessful', $this->response);
            if ($this->response->isRedirect())
                call_user_func($controller . '::isRedirect', $this->response);
        } catch (\Exception $th) {
            return $th;
        }

        $this->response = null;
    }
}
