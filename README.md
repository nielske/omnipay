# Omnipay #

**Install package**

`composer require nepp95/omnipay`

**Publish configuration**

`php artisan vendor:publish --provider="Nepp95\Omnipay\PaymentServiceProvider"`

**Add webhook route** *(Edit controller + action)*

`Route::get('/webhook', 'PaymentController@webhook')->name('webhook');`

**Create controller**

`php artisan make:payment PaymentController`

Make sure to set all the environment variables. A list is shown in the config file.

**Basic usage**
```
use Nepp95\Omnipay\Payment;

$options = [
    'amount' => '6.50',
    'description' => 'Testorder #1234',
    'transactionId' => 1234,
    'returnUrl' => route('return')
];

// Create a payment
$payment = new Payment();
$payment->setGateway('sisow');

// Initialize the gateway. Options are optional, default is taken from config.
$payment->initialize();

// Send purchase request
$response = $payment->pay(
    $options,
    '\\App\\Http\\Controllers\\PaymentController',
);
```